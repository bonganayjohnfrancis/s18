
   let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: '1999',
    price: 1000 
}; 

console.log(cellphone); 
console.log(typeof cellphone); 


let users = [
   { 
        name: 'Anna', 
        age: '23'
   },
   { 
        name: 'Nicole', 
        age: '18'
   },
   { 
        name: 'Smith', 
        age: '42'
   },
   { 
        name: 'Pam', 
        age: '13'
   },
   { 
        name: 'Anderson', 
        age: '26'
   }
  ];

console.log(users); 
console.table(users); 

let friend = {
    //properties
    firstName: 'Joe', 
    lastName: 'Smith', 
    isSingle: true, 
 emails: ['joesmith@gmail.com', 'jsmith@company.com'], 
 address: { 
     city: 'Austin',
     state: 'Texas',
     country: 'USA'
 },
 introduce: function() {
     console.log('Hello Meet my new Friend'); 
 },
 advice: function() {
     console.log('Give friend an advice to move on');
 }, 
 wingman: function() {
     console.log('Hype friend when meeting new people');
 }
}
console.log(friend);

    


function Laptop(name, year, model) {
   this.brand = year;
   this.manufacturedOn = name; 
   this.model = model; 
}; 



let laptop1 = new Laptop('Sony', 2008, 'viao');
let laptop2 = new Laptop('Apple', 2019, 'Mac'); 
let laptop3 = new Laptop('HP', 2015, 'hp'); 


let gadgets = [laptop1, laptop2, laptop3]; 
console.table(gadgets); 


function Pokemon(name, type, level, trainer) {
    this.pokemonName = name; 
    this.pokemonType = type; 
    this.pokemonHealth = 2 * level;  
    this.pokemonLevel = level; 
    this.owner = trainer; 
    this.tackle = function(target){
        console.log(this.pokemonName + ' tackled ' + target.pokemonName); 
    };
    this.greetings = function() {
        console.log(this.pokemonName + ' says Hello! '); 
    };
}

let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash ketchup');
let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty');
let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak'); 

let pokemons = [pikachu, ratata, snorlax]; 

console.table(pokemons); 


console.log(snorlax.owner); 
console.log(pikachu.pokemonType); 
pikachu.tackle(ratata); 
snorlax.greetings(); 


console.log(ratata['owner']); 
console.log(ratata['pokemonLevel']); 


let trainer = {};
console.log(trainer); 

trainer.name = 'Ash Ketchum'; 
console.log(trainer); 
trainer.friends = ['Misty', 'Brock', 'Tracey']; 
console.log(trainer); 

//
trainer.speak = function() {
   console.log('Pikachu, i choose you!');
}

trainer.speak(); 
